﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
   public float interactionRayLength = 3.0f;
    bool colission_detected;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      //  InteractRaycastAlternative();
    }
    void InteractRaycast()
    {
        Vector3 playerPosition = transform.position;
        Vector3 fowardDirection = transform.forward;

        Ray interactionRay = new Ray(playerPosition, fowardDirection);
        RaycastHit interactionRayHit;
      

        Vector3 interactionRayEndpoint = fowardDirection * interactionRayLength;
        Debug.DrawLine(playerPosition, interactionRayEndpoint);

        bool hitFound = Physics.Raycast(interactionRay, out interactionRayHit, interactionRayLength);
        if(hitFound)
        {
            GameObject hitGameobject = interactionRayHit.transform.gameObject;
            string hitFeedback = hitGameobject.name;
            Debug.Log(hitFeedback);
        }
        else
        {
            string nothingHitFeedback = "";
            Debug.Log(nothingHitFeedback);
        }
    }

   public bool InteractRaycastAlternative()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, interactionRayLength))
        {
            Debug.Log(hit.transform.gameObject.name);
            colission_detected =true;
            return colission_detected;
        }
        else
        {
            Debug.Log("-");
            colission_detected = false;
            return colission_detected;
        }
    }
}
