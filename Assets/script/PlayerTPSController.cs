﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;
    // Ary
    public PlayerInteraction interaction;
    bool interaction_wall;
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();
       interaction_wall= interaction.InteractRaycastAlternative();
        print(interaction_wall);

       // if (interaction_wall)
      //  {
       //     input.hMovement=0.0f;
           // input.vMovement = 0.0f;
        //    input.dash = false;
     //   }
        characterMovement.moveCharacter(input.hMovement,input.vMovement, cam, input.jump, input.dash,interaction_wall);
    }
}
